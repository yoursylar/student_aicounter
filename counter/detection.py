import os
import cv2
from imageai.Detection import VideoObjectDetection
from .config import  MODEL_PATH
from .helper import in_polygon


class VideoDetection:

    __MDF = 32

    def __init__(self, api):
        self.__api = api
        self.__detector = VideoObjectDetection()
        self.__detector.setModelTypeAsRetinaNet()
        self.__detector.setModelPath(MODEL_PATH)
        self.__detector.loadModel()
        self.__custom_objects = self.__detector.CustomObjects(person=True)

        self.__camera_id = None

    def process(self, cap, camera_id):
        self.__camera_id = camera_id

        self.__detector.detectCustomObjectsFromVideo(
            custom_objects=self.__custom_objects,
            camera_input=cap,
            save_detected_video=False,
            frames_per_second=1,
            detection_timeout=5,
            per_frame_function=self.__for_frame,
            video_complete_function=self.__for_full,
            minimum_percentage_probability=self.__MDF,
            log_progress=True
            #, return_detected_frame=True
        )

    def __for_frame(self, frame_number, output_array, output_count):
        print("CAM_ID ", self.__camera_id)
        print("id: {} FOR FRAME ".format(self.__camera_id), frame_number)
        print("id: {} Output for each object : ".format(self.__camera_id), output_array)
        print("id: {} Output count for unique objects : ".format(self.__camera_id), output_count)
        print("id: {} ------------END OF A FRAME --------------".format(self.__camera_id))
        try:
            self.__api.add_frame(self.__camera_id, output_array)
        except Exception as ex:
            print(ex)

    def __for_full(self, output_arrays, count_arrays, average_output_count):
        print("Array for the outputs of each frame ", output_arrays)
        print("Array for output count for unique objects in each frame : ", count_arrays)
        print("Output average count for unique objects in the entire video: ", average_output_count)
        print("------------END OF THE VIDEO --------------")

        try:
            for zone in self.__api.zones_by_camera_id(self.__camera_id):
                try:
                    count = 0
                    persons_count = 0
                    frame_count = 0
                    zone_id = zone.get('id')
                    #TODO
                    xd = zone.get('x')
                    yd = zone.get('y')
                    for frame in output_arrays:
                        frame_count += 1
                        for obj in frame:
                            x1, y1, x2, y2 = obj.get('box_points')
                            if in_polygon(x1, y1, xd, yd) \
                                    or in_polygon(x2, y2, xd, yd) \
                                    or in_polygon(x2, y1, xd, yd) \
                                    or in_polygon(x1, y2, xd, yd):
                                persons_count += 1

                    if persons_count and frame_count:
                        count = int(round(persons_count / frame_count))

                    self.__api.add_stat(zone_id,  count)

                    print(zone_id, count)
                except Exception as ex:
                    print(ex)

        except Exception as ex:
            print(ex)
