# получение полигонов
def in_polygon(x: int, y: int, xp: tuple or list, yp: tuple or list) -> bool:
    """
    Определить, находится ли точка внутри многоугольника
    :param x: координата x точки
    :param y: координата y точки
    :param xp: список координат x многоугольника (последовательный обход)
    :param yp: список координат y многоугольника (последовательный обход)
    :return:
    """
    c = 0
    for i in range(len(xp)):
        if ((yp[i] <= y < yp[i - 1]) or (yp[i - 1] <= y < yp[i])) \
                and (x > (xp[i - 1] - xp[i]) * (y - yp[i]) / (yp[i - 1] - yp[i]) + xp[i]):
            c = 1 - c
    return c > 0


print(in_polygon(1, 1, [0, 0, 4, 4], [0, 4, 4, 0]))