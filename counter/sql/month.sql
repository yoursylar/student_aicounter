
SELECT
    date_part('day', time),
    max(frame_count) max,
    min(frame_count) min,
    (sum(frame_count)/count(frame_count))::integer medium
FROM (
    SELECT
           frames.id,
           i."time",
           count(detections.id) "frame_count"
    FROM (
        SELECT
            *
        FROM generate_series('2019-12-08 00:00'::timestamp - interval '30 days', '2019-12-08 00:00'::timestamp, '1 day') "time"
    ) i
        LEFT JOIN frames
            ON frames."time" BETWEEN i."time" AND i."time" + interval '1 days'
        LEFT JOIN detections
            ON detections."frame_id" = frames."id"
            AND (point(detections.box_points[1], detections.box_points[2]) @ polygon('((70,240), (1460,90), (1910,230), (1910,1040), (1040,1040))') OR
                  point(detections.box_points[3], detections.box_points[4]) @ polygon('((70,240), (1460,90), (1910,230), (1910,1040), (1040,1040))') OR
                  point(detections.box_points[1], detections.box_points[4]) @ polygon('((70,240), (1460,90), (1910,230), (1910,1040), (1040,1040))') OR
                  point(detections.box_points[3], detections.box_points[2]) @ polygon('((70,240), (1460,90), (1910,230), (1910,1040), (1040,1040))'))
    GROUP BY frames.id, i."time"
) frames
GROUP BY time
ORDER BY time
