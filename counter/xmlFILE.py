from lxml import etree


def create_annotation(detections, folder, filename):
    _file = etree.Element('annotation')
    doc = etree.ElementTree(_file)
    folder_elt = etree.SubElement(_file, 'folder')
    folder_elt.text = folder
    filename_elt = etree.SubElement(_file, 'filename')
    filename_elt.text = filename
    path_elt = etree.SubElement(_file, 'path')
    path_elt.text = 'C:.......'

    source = etree.SubElement(_file, 'source')
    source_elt = etree.SubElement(source, 'database')
    source_elt.text = 'Unknown'

    _size = etree.SubElement(_file, 'size')
    _sizeElt = etree.SubElement(_size, 'size')
    width_elt = etree.SubElement(_size, 'width')
    width_elt.text = '000'
    heigth_elt = etree.SubElement(_size, 'heigth')
    heigth_elt.text = '001'
    depth_elt = etree.SubElement(_size, 'depth')
    depth_elt.text = '002'

    segmented_elt = etree.SubElement(_file, 'segmented')
    segmented_elt.text = '0'
    _object = etree.SubElement(_file, 'object')

    for box in detections:
        _object = etree.SubElement(_file, 'object')
        name_elt = etree.SubElement(_object, 'name')
        name_elt.text = 'people'
        pose_elt = etree.SubElement(_object, 'pose')
        pose_elt.text = 'Unspecified'
        truncated_elt = etree.SubElement(_object, 'truncated')
        truncated_elt.text = '0'
        difficult_elt = etree.SubElement(_object, 'difficult')
        difficult_elt.text = '0'

        bndbox = etree.SubElement(_object, 'bndbox')
        xmin_elt = etree.SubElement(bndbox, 'xmin')
        xmin_elt.text = str(box[0])
        ymin_elt = etree.SubElement(bndbox, 'ymin')
        ymin_elt.text = str(box[1])
        xmax_elt = etree.SubElement(bndbox, 'xmax')
        xmax_elt.text = str(box[2])
        ymin_elt = etree.SubElement(bndbox, 'ymax')
        ymin_elt.text = str(box[3])

    out_file = open('img_n.xml', 'wb')
    doc.write(out_file)


box_points = [
    [1,2,3,4],
    [10,20,30,40],
    [100,200,300,400],
    [1001,2001,3001,4001]
]
f_name = 'f_name'
folder_name = 'folфывdedfhg_name'


create_annotation(box_points, folder_name, f_name)

