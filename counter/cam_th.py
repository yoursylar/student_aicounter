import random
import time
import cv2
from .config import VIDEO_URL
from threading import Thread
from .detection import VideoDetection


class Camera:

    __NEED_SHOW = False

    def __init__(self, detector, cam_id, name, pause=60):
        """Инициализация потока"""
        self.__detector = detector
        self.__cam_id = cam_id
        self.name = name
        self.__pause = pause

    def run(self):
        """Запуск потока"""
        if self.__NEED_SHOW:
            cv2.namedWindow(self.name)
        cap = cv2.VideoCapture(VIDEO_URL.format(self.name))
        self.__detector.process(cap, self.__cam_id)
