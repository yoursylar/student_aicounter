import time

from .ai_api import AiApi
from .cam_th import Camera
from .detection import VideoDetection


class AiCounter:

    __SLEEP_SECONDS = 15

    def __init__(self):
        self.__api = AiApi()
        self.__cameras = []
        self.__detector = VideoDetection(self.__api)

    def run(self, pause=60):
        if not self.__cameras:
            for cam in self.__api.cameras:
                self.__cameras.append(Camera(self.__detector, cam.get('id'), cam.get('link'), pause))
        for c_th in self.__cameras:
            c_th.run()

        time.sleep(pause or self.__SLEEP_SECONDS)
        self.run()
