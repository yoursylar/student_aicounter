import os


VIDEO_URL = 'rtmp://test-video-ai/{}/stream'
EXEC_PATH = os.getcwd()
MODEL_PATH = os.path.join(EXEC_PATH, 'counter\\models\\resnet50_coco_best_v2.0.1.h5')
BASE_URL = 'https://fix-online.sbis.ru/aicounter/'

