import requests
from .config import BASE_URL
from datetime import datetime
import json


class AiApi:
    __ZONES_URL = BASE_URL + 'zone/get_all'
    __CAMERA_URL = BASE_URL + 'camera/get_all'

    STAT_URL = BASE_URL + 'stat/add'
    FRAME_ADD_URL = BASE_URL + 'frame/add'

    def __init__(self):
        self.__zones = None
        self.__cameras = None
        # self.__frame = -1
        # self.__stat = -1

    @property
    def zones(self):
        """Список зон"""
        if self.__zones is None:
            self.__zones = requests.get(self.__ZONES_URL).json()
        return self.__zones

    @property
    def cameras(self):
        """Список камер"""
        if self.__cameras is None:
            self.__cameras = requests.get(self.__CAMERA_URL).json()
        return self.__cameras

    def zones_by_camera_id(self, cam_id):
        for zone in self.zones:
            if zone.get('camera') == cam_id:
                yield zone

    def add_frame(self, cam_id, output_array):
        r = requests.post(self.FRAME_ADD_URL, data={
            'camera_id': cam_id,
            'time': datetime.now(),
            'box_points': json.dumps([detection.get('box_points') for detection in output_array])})

        print(r)

    def add_stat(self, zone_id,  count):
        requests.post(self.STAT_URL, data={
            'zone_id': zone_id,
            'event_time': datetime.now(),
            'people_count': count})
