from app import *
import view
import login

app.secret_key = "secret"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
