from flask import Flask, render_template, jsonify


app = Flask(__name__, static_folder='static', template_folder='templates')

@app.route('/')
def run():
    return render_template('new_index.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=31337, debug=True)