from datetime import datetime, timedelta
from models.Camera import *
from models.Stat import *
from models.Zone import *
from models.Deterns import *
from models.frame import *
from models.detection import *
from app import *
import json
from flask_login import current_user


@app.route('/')
def index():
    return render_template('index.html')    


@app.route('/zone', methods=['GET', 'POST'])
def zone():

    data = Zone.query.all()
    data = jsonify([e.serialize() for e in data])
    return data


@app.route('/get_stat', methods=['GET','POST'])
def get_data():

    # zone_id = request.form.get('zone_id')
    # toDate = request.form.get('time_start')
    # period = request.form.get('period')  

    zone_id = 0
    a = [int(i) for i in "2019-11-28".split('-')]
    toDate = datetime(a[0], a[1], a[2])
    period = 'week'

    if period == 'day':
        fromDate = str(toDate + timedelta(days=-1))[:10]

    elif period == 'week':
        fromDate = str(toDate + timedelta(days=-7))[:10]

    elif period == 'month': 
        fromDate = str(toDate + timedelta(days=-30))[:10]

    toDate = str(toDate)[:10]

    print(fromDate, toDate)

    if zone_id == 0:
        Stats = Stat.query.order_by(Stat.date_column).all()
        stat = db.engine.execute(f"select * from Stat where date_column >= '{fromDate}' and date_column <= '{toDate}'").fetchall()
    else:
        stat = db.engine.execute(f"select * from Stat where date_column >= '{fromDate}' and date_column < '{toDate}' and zone_id = '{zone_id}'").fetchall()

    answer = {}


    count_zones = db.engine.execute(f"select Zone.id from Zone").fetchall()
    answer = {}
    for rowproxy in count_zones:
        for k in rowproxy:
            answer[k] = {}

    for i in answer.keys():

        if period == 'day':
            daily_index, index = 0, 0
            date = stat[0][2]
            for j in range(len(stat)):

                if stat[j][1] == i:
                    if stat[j][2] == date:
                        answer[i][index] = {}
                        answer[i][index]['zone_id'] = stat[j][1]
                        answer[i][index]['date_column'] = stat[j][2]
                        answer[i][index]['time_column'] = stat[j][3]
                        answer[i][index]['count'] = stat[j][4]
                        index+=1
                    else:
                        index = 0
                           

        else:
            daily_count, daily_index, index = 0, 0, 0
            date = stat[0][2]
            answer[i][daily_index] = {}
            for j in range(len(stat)):

                if stat[j][1] == i:
                    daily_count += stat[j][4]
                    if stat[j][2] == date:
                        index+=1
                        answer[i][daily_index]['zone_id'] = stat[j][1]
                        answer[i][daily_index]['date_column'] = stat[j][2]
                        answer[i][daily_index]['count'] = int(daily_count/index)
                    else:
                        index = 0
                        daily_index += 1
                        daily_count = 0 
                        answer[i][daily_index] = {}
                        date = stat[j][2] 
    return answer


@app.route('/zone/get_all', methods=['GET'])
def apizonegetall():
    data = Zone.query.filter(Zone.user_id == None).all()
    data = jsonify([e.serialize() for e in data])
    return data


@app.route('/zone/add', methods=['POST'])
def apiaddpoints():
    app.logger.debug("start")
    cam_id = int(request.form['camera_id'])
    capacity = int(request.form['capacity'])
    name = request.form['name']
    arrx = request.form['x']
    arry = request.form['y']
    rez = Zone(None, cam_id, capacity, name, arrx, arry)
    app.logger.debug("End")
    return str(rez.id)


@app.route('/camera/get_all')
def camera_get_all():
    try:
        cameras = Camera.query.all()
        return jsonify([e.serialize() for e in cameras])
    except Exception as e:
        return str(e)


@app.route('/frame/add', methods=['POST'])
def add_frame():
    if not request.form['camera_id']:
        flash('camera_id is required', 'error')

    camera_id = request.form['camera_id']
    time = request.form['time'] if 'time' in request.form else datetime.now()
    box_points = json.loads(request.form['box_points'] if 'box_points' in request.form else '[]')
    try:
        frame = Frame(camera_id=camera_id, time=time)
        db.session.add(frame)
        db.session.commit()
        for box in box_points:
            detection = Detection(frame_id=frame.id, box_points=box, time=time)
            db.session.add(detection)
            db.session.commit()
        db.session.commit()
        return str(frame.id)
    except Exception as e:
        return str(e)


@app.route('/stat/add', methods=['POST'])
def add_stat():
    if not request.form['zone_id']:
        flash('zone_id is required', 'error')
    if not request.form['people_count']:
        flash('people_count is required', 'error')

    zone_id = request.form['zone_id']
    event_time = request.form['event_time'] if 'event_time' in request.form else datetime.now()
    people_count = request.form['people_count']
    try:
        stat = Stat(zone_id, event_time, people_count)
        db.session.add(stat)
        db.session.commit()
        return 'Stats added with data zone_id={} event_time={}'.format(stat.zone_id, stat.event_time)
    except Exception as e:
        return str(e)


@app.route('/stat/get_all', methods=['POST'])
def stat_charts():
    if not request.form['interval']:
        flash('interval is required', 'error')

    if not request.form['date']:
        flash('date is required', 'error')

    zone_id = request.form['zone'] if 'zone' in request.form else None
    if not zone_id or zone_id == '' or zone_id == 'None':
        zone_id = None
    interval_type = request.form['interval']
    param_date = datetime.strptime(request.form['date'], '%Y-%m-%d')
    if interval_type == 'day':
        start_date = param_date
        end_date = start_date + timedelta(days=1)
        date_part = 'HOUR'
        date_parts = '1 HOURS'
        date_parts_const = '1 HOURS'
        period = '1 HOUR'
        interval_value = '23 hours'
        step = timedelta(hours=1)
        limit_val = 24
    elif interval_type == 'week':
        curr_date = param_date
        start_date = curr_date - timedelta(days=6)
        end_date = curr_date + timedelta(days=1)
        date_part = 'DAY'
        date_parts = '1 DAYS'
        date_parts_const = '1 DAYS'
        period = '1 DAY'
        interval_value = '6 days'
        step = timedelta(days=1)
        limit_val = 7
    elif interval_type == 'month':
        curr_date = param_date
        start_date = curr_date - timedelta(days=29)
        end_date = curr_date + timedelta(days=1)
        date_part = 'DAY'
        date_parts = '1 DAYS'
        date_parts_const = '1 DAYS'
        period = '1 DAY'
        interval_value = '29 days'
        step = timedelta(days=1)
        limit_val = 30
    else:
        flash('unknown interval_type value', 'error')

    coord = request.form['coord'] if 'coord' in request.form else None

    if True == True:
        if coord is None:
            series_cte = (db.select([
                db.func.generate_series(start_date, end_date, period).label('period')
            ]).limit(limit_val).cte('series_cte'))
            periods_cte = (db.select(
                [series_cte.c.period.label('period_start'),
                 (series_cte.c.period + step).label('period_end')]
            ).cte('periods_cte'))
            zone_periods = db.select([
                periods_cte.c.period_start,
                periods_cte.c.period_end,
                Zone.id
            ]).where(db.or_(Zone.id == zone_id, zone_id is None)).cte('zone_periods')
            statement = db.session.query(
                db.func.date_part(date_part, zone_periods.c.period_start),
                db.func.coalesce(db.func.max(Stat.people_count), 0),
                db.func.coalesce(db.func.min(Stat.people_count), 0),
                db.func.coalesce(db.func.sum(Stat.people_count) / db.func.nullif(db.func.count(Stat.people_count), 0), 0)
            ).outerjoin(Stat,
                        db.and_(
                            db.and_(
                                Stat.event_time.between(zone_periods.c.period_start, zone_periods.c.period_end),
                                Stat.people_count > 0
                            ), db.or_(Stat.zone_id == zone_id, zone_id is None)
                        ))
            stat_res = statement.group_by(zone_periods.c.period_start).order_by(zone_periods.c.period_start).all()
        else:
            camera_id = request.form['camera_id']
            polygons = '((70,240), (1460,90), (1910,230), (1910,1040), (1040,1040))'
            if interval_type == 'day':
                stat_res = db.engine.execute(f'''
    SELECT
        date_part('{date_part}', time),
        max(frame_count) max,
        min(frame_count) min,
        (sum(frame_count)/count(frame_count))::integer medium
    
    FROM (
        SELECT
               frames.id,
               i."time",
               count(detections.id) "frame_count"
        FROM (
            SELECT
                *
            FROM generate_series('{param_date}'::timestamp, '{param_date}'::timestamp + interval '{interval_value}', '{date_parts}') "time"
        ) i
            LEFT JOIN frames
                ON frames."time" BETWEEN i."time" AND i."time" + interval '{date_parts}' AND frames.camera_id = {camera_id}
            LEFT JOIN detections
                ON detections."frame_id" = frames."id"
                AND (point(detections.box_points[1], detections.box_points[2]) @ polygon('{polygons}') OR
                      point(detections.box_points[3], detections.box_points[4]) @ polygon('{polygons}') OR
                      point(detections.box_points[1], detections.box_points[4]) @ polygon('{polygons}') OR
                      point(detections.box_points[3], detections.box_points[2]) @ polygon('{polygons}'))
        GROUP BY frames.id, i."time"
    ) frames
    GROUP BY time
    ORDER BY time
                ''')
            else:
                stat_res = db.engine.execute(f'''
SELECT
    date_part('{date_part}', time),
    max(frame_count) max,
    min(frame_count) min,
    (sum(frame_count)/count(frame_count))::integer medium
FROM (
    SELECT
           frames.id,
           i."time",
           count(detections.id) "frame_count"
    FROM (
        SELECT
            *
        FROM generate_series('{param_date}'::timestamp - interval '{interval_value}', '{param_date}'::timestamp, '{date_parts}') "time"
    ) i
        LEFT JOIN frames
            ON frames."time" BETWEEN i."time" AND i."time" + interval '{date_parts}' AND frames.camera_id = {camera_id}
        LEFT JOIN detections
            ON detections."frame_id" = frames."id"
            AND (point(detections.box_points[1], detections.box_points[2]) @ polygon('{polygons}') OR
                  point(detections.box_points[3], detections.box_points[4]) @ polygon('{polygons}') OR
                  point(detections.box_points[1], detections.box_points[4]) @ polygon('{polygons}') OR
                  point(detections.box_points[3], detections.box_points[2]) @ polygon('{polygons}'))
    GROUP BY frames.id, i."time"
) frames
GROUP BY time
ORDER BY time
                ''')
        zones_res = Stat.query.with_entities(
            Zone.name,
            Stat.people_count
        ).join(Zone).order_by(Stat.event_time.desc()).limit(5).all()

        period_vals = []
        min_vals = []
        middle_vals = []
        max_vals = []
        zone_vals = []

        for el in stat_res:
            period_vals.append(el[0])
            min_vals.append(el[2])
            max_vals.append(el[1])
            middle_vals.append(el[3])

        for el in zones_res:
            zone_vals.append({'name': el[0], 'y': el[1]})

        return {
            'period': period_vals,
            'min': min_vals,
            'max': max_vals,
            'middle': middle_vals,
            'zones': zone_vals
        }
    #except Exception as e:
    #    return str(e)
