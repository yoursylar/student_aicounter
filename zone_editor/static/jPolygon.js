var perimetr = new Array();
var complete = false;
var canvas = document.getElementById('jPolygon');
var ctx;
var xhr = new XMLHttpRequest();

function line_intersects(p0, p1, p2, p3) {
  var s1_x, s1_y, s2_x, s2_y;
  s1_x = p1['x'] - p0['x'];
  s1_y = p1['y'] - p0['y'];
  s2_x = p3['x'] - p2['x'];
  s2_y = p3['y'] - p2['y'];

  var s, t;
  s = (-s1_y * (p0['x'] - p2['x']) + s1_x * (p0['y'] - p2['y'])) / (-s2_x * s1_y + s1_x * s2_y);
  t = ( s2_x * (p0['y'] - p2['y']) - s2_y * (p0['x'] - p2['x'])) / (-s2_x * s1_y + s1_x * s2_y);

  if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
  {
      // Collision detected
      return true;
  }
  return false; // No collision
}

function point(x, y){
  ctx.fillStyle="white";
  ctx.strokeStyle = "white";
  ctx.fillRect(x-2,y-2,4,4);
  ctx.moveTo(x,y);
}

function undo(){
  ctx = undefined;
  perimeter.pop();
  complete = false;
  start(true);
}

function clear_canvas(){
  ctx = undefined;
  perimeter = new Array();
  complete = false;
  document.getElementById('coordinates').value = '';
  start();
}
function draw(end){
  ctx.lineWidth = 1;
  ctx.strokeStyle = "white";
  ctx.lineCap = "square";
  ctx.beginPath();

  for(var i=0; i<perimeter.length; i++){
      if(i==0){
          ctx.moveTo(perimeter[i]['x'],perimeter[i]['y']);
          end || point(perimeter[i]['x'],perimeter[i]['y']);
      } else {
          ctx.lineTo(perimeter[i]['x'],perimeter[i]['y']);
          end || point(perimeter[i]['x'],perimeter[i]['y']);
      }
  }
  if(end){
      ctx.lineTo(perimeter[0]['x'],perimeter[0]['y']);
      ctx.closePath();
      ctx.fillStyle = 'rgba(255, 0, 0, 0.5)';
      ctx.fill();
      ctx.strokeStyle = 'blue';
      complete = true;
  }
  ctx.stroke();

  // print coordinates
  if(perimeter.length == 0){
      document.getElementById('coordinates').value = '';
  } else {
      document.getElementById('coordinates').value = JSON.stringify(perimeter);
  }
}

function check_intersect(x,y){
  if(perimeter.length < 4){
      return false;
  }
  var p0 = new Array();
  var p1 = new Array();
  var p2 = new Array();
  var p3 = new Array();

  p2['x'] = perimeter[perimeter.length-1]['x'];
  p2['y'] = perimeter[perimeter.length-1]['y'];
  p3['x'] = x;
  p3['y'] = y;

  for(var i=0; i<perimeter.length-1; i++){
      p0['x'] = perimeter[i]['x'];
      p0['y'] = perimeter[i]['y'];
      p1['x'] = perimeter[i+1]['x'];
      p1['y'] = perimeter[i+1]['y'];
      if(p1['x'] == p2['x'] && p1['y'] == p2['y']){ continue; }
      if(p0['x'] == p3['x'] && p0['y'] == p3['y']){ continue; }
      if(line_intersects(p0,p1,p2,p3)==true){
          return true;
      }
  }
  return false;
}

function save() {
    if(complete){
        alert('Polygon already created');
        return false;
    }
    if(perimeter.length <= 2){
        alert('You need at least three points for a polygon');
        return false;
    }
    draw(true);
    var saveButton = document.getElementById('save_btn');
    saveButton.style.visibility = 'visible';
      alert('Polygon closed');
      event.preventDefault();
      return false;
}

function point_it(event) {
  if(complete){
      alert('Polygon already created');
      return false;
  }
  var rect, x, y;

  if(event.ctrlKey || event.which === 3 || event.button === 2){
      if(perimeter.length <= 2){
          alert('You need at least three points for a polygon');
          return false;
      }
      x = perimeter[0]['x'];
      y = perimeter[0]['y'];
      if(check_intersect(x,y)){
          alert('The line you are drowing intersect another line');
          return false;
      }
      draw(true);
      alert('Polygon closed');
      event.preventDefault();
      return false;
  } else {
      rect = canvas.getBoundingClientRect();
      x = event.clientX - rect.left;
      y = event.clientY - rect.top;
      if (perimeter.length>0 && x == perimeter[perimeter.length-1]['x'] && y == perimeter[perimeter.length-1]['y']){
          // same point - double click
          return false;
      }
      if(check_intersect(x,y)){
          alert('The line you are drowing intersect another line');
          return false;
      }
      perimeter.push({'x':x,'y':y});
      draw(false);
      return false;
  }
}

function init() {
    var zoneSelect = document.getElementById('zoneSelect');
    zoneSelect.options.length = 1;
    var body;
    var saveButton = document.getElementById('save_btn');
    saveButton.style.visibility = 'hidden';
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            body = JSON.parse(this.response)
            console.log(body)
            
            body.zones.forEach(zone => {
                var option = document.createElement("option");
                option.appendChild(document.createTextNode('zoneId'));
                option.setAttribute('zoneId', zone.zoneId);
                option.appendChild(document.createTextNode('userId'));
                option.setAttribute('userId', zone.userId);
                option.appendChild(document.createTextNode('cameraId'));
                option.setAttribute('cameraId', zone.cameraId);
                option.appendChild(document.createTextNode('imagePath'));
                option.setAttribute('imagePath', zone.imagePath);
                option.appendChild(document.createTextNode('coordinates'));
                option.setAttribute('coordinates', JSON.stringify(zone.coordinates));
                option.text = zone.zoneId;
                zoneSelect.add(option);
            });
        }
    };  

    xhr.open('GET', 'http://127.0.0.1:5000/server/zones');
    xhr.send();
}

function selected_zone(temp) {
    var selectedIndex = document.getElementById('zoneSelect').selectedIndex;
    canvas.setAttribute('href', temp[selectedIndex].getAttribute('imagePath'));
    var drawerContainer = document.getElementById('drawer-container');
    drawerContainer.style.visibility = 'visible';
    clear_canvas();
    var coordinates = JSON.parse(temp[selectedIndex].getAttribute('coordinates'));
    console.log(coordinates)
    if(coordinates != undefined && coordinates != null)
        drawExisting(coordinates)
}

function drawExisting(coordinates){
    clear_canvas();
    perimeter = coordinates;
    start(true);
}

function send() {

    xhr.open("POST", 'http://127.0.0.1:5000/server/load',true);
    xhr.setRequestHeader("Content-type","application/json");

    xhr.send(JSON.stringify(perimeter))
    xhr.abort();

    
    init(); 
}

function start(with_draw) {
  var img = new Image();
  img.src = canvas.getAttribute('href');

  img.onload = function() {
    ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    if(with_draw == true){
        draw(false);
  }
  }
}