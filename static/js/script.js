// Инициализируем графики, после только конфиги меняем
var chart_line = Highcharts.chart('graph-line', {

    title: {
        text: 'График посещений зала'
    },

    yAxis: {
        title: {
            text: 'Количество посетителей'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false,
                
            }
        },
        line: {
            dataLabels: {
                enabled: true,
                inside: false
            }
        }
    },

    series: [{
        name: 'Максимум',
        data: []
    }, {
        name: 'Средняя',
        data: []
    }, {
        name: 'Минимум',
        data: []
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

var chart_circle = Highcharts.chart('graph-zone', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Текущая загруженность зон'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                connectorColor: 'silver'
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Зоны',
        colorByPoint: true,
        data: [ 
        {
            name: 'Зона 1',
            y: 0
        }, {
            name: 'Зона 2',
            y: 0
        }, {
            name: 'Зона 3',
            y: 0
        }, {
            name: 'Зона 4',
            y: 0
        }
        ]
    }]
});

 $('#input-date-control').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
});

site_api = 'https://fix-online.sbis.ru/aicounter/' // 'http://test-video-ai.unix.tensor.ru:8000';

var 
    gr_zone = document.getElementById('select-zone-control'),
    gr_interval = document.getElementById('select-interval-control'),
    gr_camera = document.getElementById('select-camera-control'),
    gr_date = document.getElementById('input-date-control');

// Заполняем селект зонами
var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        var resp = JSON.parse(this.responseText);
        for(var i = 0; i < resp.length;i++){
            var opt = document.createElement('option');
            opt.value = resp[i]['id'];
            opt.innerHTML = resp[i]['name'];
            gr_zone.appendChild(opt);
        }
    }
});
xhr.open("GET", site_api + "/zone/get_all");
xhr.send();

var reloadGraph = function() { 
    var 
        data = new FormData(),
        xhr = new XMLHttpRequest();

    data.append("interval", gr_interval.options[gr_interval.selectedIndex].value);
    data.append("date", gr_date.value);
    data.append("zone", gr_zone.value);
        
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {

            var resp = JSON.parse(this.responseText);  
            chart_circle.update({series: [{data: resp['zones']}]});
            chart_line.update({
                series: [{
                    name: 'Максимум',
                    data: resp['max']
                }, {
                    name: 'Средняя',
                    data: resp['middle']
                }, {
                    name: 'Минимум',
                    data: resp['min']
                }],
                xAxis: {
                    categories: resp['period']
                }
            });
        }
    });
    xhr.open("POST", site_api + "/stat/get_all");
    xhr.send(data);    
}

setInterval(reloadGraph, 1000);

/////////////////////
var 
    cameras = [
        {0: '/aicounter/static/img/camera_0.jpg'},
        {1: '/aicounter/static/img/camera_1.jpeg'},
        {2: '/aicounter/static/img/camera_2.jpg'},
        {3: '/aicounter/static/img/camera_3.jpg'}  
    ];

var 
    jPolygon = document.getElementById('jPolygon'),
    jPolygon_canvas = jPolygon.getContext("2d"),
    jPolygon_drawer = document.getElementById('jPolygon_drawer'),
    jPolygon_draw = document.getElementById('jPolygon_draw'),
    complete = false,
    perimeter = new Array();


cameras.forEach(function(value, key){
    var 
        option = document.createElement('option');

    option.value = key;
    option.innerHTML = 'Камера '+(key + 1);
    option.setAttribute('image', value[key]);

    gr_camera.appendChild(option);
});

var drawUserGraph = function() {

    var 
        data = new FormData(),
        gr_camera = document.getElementById('select-camera-control'),
        xhr = new XMLHttpRequest();

    data.append("interval", gr_interval.options[gr_interval.selectedIndex].value);
    data.append("date", gr_date.value);
    data.append('camera_id', gr_camera.options[gr_camera.selectedIndex].value);
    console.log(perimeter)
    data.append("coord", JSON.stringify(perimeter));
    
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {

            var chart_user_line = Highcharts.chart('graph-user-line', {

                title: {
                    text: 'График посещений зала по вашим кооридинатам'
                },

                yAxis: {
                    title: {
                        text: 'Количество посетителей'
                    }
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false,
                            
                        }
                    },
                    line: {
                        dataLabels: {
                            enabled: true,
                            inside: false
                        }
                    }
                },

                series: [{
                    name: 'Максимум',
                    data: []
                }, {
                    name: 'Средняя',
                    data: []
                }, {
                    name: 'Минимум',
                    data: []
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });

            var resp = JSON.parse(this.responseText);  
            chart_user_line.update({
                series: [{
                    name: 'Максимум',
                    data: resp['max']
                }, {
                    name: 'Средняя',
                    data: resp['middle']
                }, {
                    name: 'Минимум',
                    data: resp['min']
                }],
                xAxis: {
                    categories: resp['period']
                }
            });
        } 
    });

    xhr.open("POST", site_api + "/stat/get_all");
    xhr.send(data);    
};

var camerasOnChange = function(el){
    var 
        imageObj = new Image();
    
  
    imageObj.onload = function() {

        jPolygon_canvas.drawImage(imageObj, 0, 0);
        /*
        jPolygon.height = jPolygon.width * (imageObj.height / imageObj.width);
        var oc = document.createElement('canvas'),
            octx = oc.getContext('2d');

        oc.width = imageObj.width * 0.5;
        oc.height = imageObj.height * 0.5;
        octx.drawImage(imageObj, 0, 0, oc.width, oc.height);
        octx.drawImage(oc, 0, 0, oc.width * 0.5, oc.height * 0.5);
        jPolygon_canvas.drawImage(imageObj, 0, 0, oc.width * 0.5, oc.height * 0.5, 0, 0, jPolygon.width, jPolygon.height);
        */

    };
    document.getElementById('canvas-control').style.display = "flex";
    imageObj.src = el[el.selectedIndex].getAttribute('image');
};



function line_intersects(p0, p1, p2, p3) {
  var s1_x, s1_y, s2_x, s2_y;
  s1_x = p1['x'] - p0['x'];
  s1_y = p1['y'] - p0['y'];
  s2_x = p3['x'] - p2['x'];
  s2_y = p3['y'] - p2['y'];

  var s, t;
  s = (-s1_y * (p0['x'] - p2['x']) + s1_x * (p0['y'] - p2['y'])) / (-s2_x * s1_y + s1_x * s2_y);
  t = ( s2_x * (p0['y'] - p2['y']) - s2_y * (p0['x'] - p2['x'])) / (-s2_x * s1_y + s1_x * s2_y);

  if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
  {
      // Collision detected
      return true;
  }
  return false; // No collision
}

function point(x, y){
    jPolygon_canvas.fillStyle="white";
    jPolygon_canvas.strokeStyle = "white";
    jPolygon_canvas.fillRect(x-2,y-2,4,4);
    jPolygon_canvas.moveTo(x,y);
}

function undo(){
  jPolygon_canvas = undefined;
  perimeter.pop();
  complete = false;
}

function clear_canvas(){
  jPolygon_canvas = undefined;
  perimeter = new Array();
  complete = false;
}

function draw(end){
  jPolygon_canvas.lineWidth = 1;
  jPolygon_canvas.strokeStyle = "white";
  jPolygon_canvas.lineCap = "square";
  jPolygon_canvas.beginPath();

  for(var i=0; i<perimeter.length; i++){
      if(i==0){
          jPolygon_canvas.moveTo(perimeter[i]['x'], perimeter[i]['y']);
          end || point(perimeter[i]['x'], perimeter[i]['y']);
      } else {
          jPolygon_canvas.lineTo(perimeter[i]['x'],perimeter[i]['y']);
          end || point(perimeter[i]['x'],perimeter[i]['y']);
      }
  }
  if(end){
      jPolygon_canvas.lineTo(perimeter[0]['x'],perimeter[0]['y']);
      jPolygon_canvas.closePath();
      jPolygon_canvas.fillStyle = 'rgba(255, 0, 0, 0.5)';
      jPolygon_canvas.fill();
      jPolygon_canvas.strokeStyle = 'blue';
      complete = true;
  }
  jPolygon_canvas.stroke();
}

function check_intersect(x,y){
  if(perimeter.length < 4){
      return false;
  }
  var p0 = new Array();
  var p1 = new Array();
  var p2 = new Array();
  var p3 = new Array();

  p2['x'] = perimeter[perimeter.length-1]['x'];
  p2['y'] = perimeter[perimeter.length-1]['y'];
  p3['x'] = x;
  p3['y'] = y;

  for(var i=0; i<perimeter.length-1; i++){
      p0['x'] = perimeter[i]['x'];
      p0['y'] = perimeter[i]['y'];
      p1['x'] = perimeter[i+1]['x'];
      p1['y'] = perimeter[i+1]['y'];
      if(p1['x'] == p2['x'] && p1['y'] == p2['y']){ continue; }
      if(p0['x'] == p3['x'] && p0['y'] == p3['y']){ continue; }
      if(line_intersects(p0,p1,p2,p3)==true){
          return true;
      }
  }
  return false;
}

function save() {
    if(complete){
        alert('Полигон уже создан');
        return false;
    }
    if(perimeter.length <= 2){
        alert('Вам нужно как минимум три точки для многоугольника');
        return false;
    }
    draw(true);
    var saveButton = document.getElementById('save_btn');
    saveButton.style.visibility = 'visible';
      drawUserGraph();
      event.preventDefault();
      return false;
}

function point_it(event) {
  if(complete){
      alert('Полигон уже создан');
      return false;
  }
  var rect, x, y;
  
  if(event.ctrlKey || event.which === 3 || event.button === 2){
      if(perimeter.length <= 2){
          alert('Вам нужно как минимум три точки для многоугольника');
          return false;
      }
      x = perimeter[0]['x'];
      y = perimeter[0]['y'];

      if(check_intersect(x,y)){
          alert('Линия, которую вы рисуете, пересекает другую линию! :( ');
          return false;
      }
      draw(true);
      drawUserGraph();
      event.preventDefault();
      return false;
  } else {
      rect = jPolygon.getBoundingClientRect();
      
      x = parseInt(event.clientX - rect.left);
      y = parseInt(event.clientY - rect.top);

      if (perimeter.length>0 && x == perimeter[perimeter.length-1]['x'] && y == perimeter[perimeter.length-1]['y']){
          // same point - double click
          return false;
      }
      if(check_intersect(x,y)){
          alert('Линия, которую вы рисуете, пересекает другую линию');
          return false;
      }
      perimeter.push({'x':x,'y':y});
      draw(false);
      return false;
  }
}


