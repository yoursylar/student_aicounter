from flask_login import login_user, logout_user, current_user
from flask import redirect, url_for, Response
from models.users import *

from app import *

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.session_protection = "strong"


@login_manager.user_loader
def load_user(id):
    return Users.query.get(int(id))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return Response('''
                        <form action="" method="post">
                            <p><input type=text name=email>
                            <p><input type=password name=password>
                            <p><input type=submit value=Login>
                        </form>
                    ''')
    email = request.form['email']
    password = request.form['password']
    remember_me = False
    if 'remember_me' in request.form:
        remember_me = True
    registered_user = Users.query.filter_by(email=email, password=password).first()
    if registered_user is None:
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('login'))
    login_user(registered_user)
    flash('Logged in successfully')
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('/login'))


@app.route("/signup", methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        return Response('''
                        <form action="" method="post">
                            <p><input type=text name=email>
                            <p><input type=password name=password>
                            <p><input type=submit value=SignUp>
                        </form>
                    ''')
    email = request.form['email']
    password = request.form['password']
    db.engine.execute(f"insert into Users (email, password) values ('{email}', '{password}')")
    flash('User successfully registered')
    return redirect(url_for('login'))
