from datetime import datetime
from app import db


class Frame(db.Model):
    __tablename__ = 'frames'

    id = db.Column(db.Integer, primary_key=True)
    camera_id = db.Column(db.Integer, db.ForeignKey('cameras.id'), nullable=False)
    time = db.Column(db.DateTime, nullable=False, default=datetime.now())

    def __init__(self, camera_id, time=None):
        self.camera_id = camera_id
        self.time = time

    def __repr__(self):
        return '<id {} camera {} time {}>'.format(self.id,
                                                  self.camera_id,
                                                  self.time)

    def serialize(self):
        return {
            'id': self.id,
            'camera_id': self.camera_id,
            'time': self.time
        }

