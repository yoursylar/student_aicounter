from app import *
class Zone(db.Model):
    __tablename__ = 'zones'

    id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    camera = db.Column(db.Integer, db.ForeignKey('cameras.id'))
    capacity = db.Column(db.Integer)
    name = db.Column(db.String(1000))
    x = db.Column(ARRAY(db.Integer))
    y = db.Column(ARRAY(db.Integer))

    def __init__(self, user_id, camera_id, capacity, name, x, y):
        self.user_id = user_id
        self.camera = camera_id
        self.capacity = capacity
        self.name = name
        self.x = x
        self.y = y

    def serialize(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'camera': self.camera,
            'name': self.name,
            'x': self.x,
            'y': self.y
        }

    def get_id(self):
        return self.id
