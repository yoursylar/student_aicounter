from app import *
class Camera(db.Model):
    __tablename__ = 'cameras'
    
    id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    name = db.Column(db.Unicode())
    link = db.Column(db.String(1000), unique = True)

    def __init__(self, link, name=None):
        self.link = link
        self.name = name

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'link': self.link
        }
