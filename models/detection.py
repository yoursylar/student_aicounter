from datetime import datetime
from app import db


class Detection(db.Model):
    __tablename__ = 'detections'

    id = db.Column(db.Integer, primary_key=True)
    frame_id = db.Column(db.Integer, db.ForeignKey('frames.id'), nullable=False)
    name = db.Column(db.Unicode())
    box_points = db.Column(db.ARRAY(db.Integer))
    time = db.Column(db.DateTime, nullable=False, default=datetime.now())

    def __init__(self, frame_id, name=None, box_points=None, time=None):
        self.frame_id = frame_id
        self.name = name
        self.box_points = box_points
        self.time = time

    def __repr__(self):
        return '<id {} name {} frame {} box_points {} time {}>'.format(self.id,
                                                                       self.name,
                                                                       self.frame_id,
                                                                       self.box_points,
                                                                       self.time)

    def serialize(self):
        return {
            'id': self.id,
            'frame_id': self.frame_id,
            'name': self.name,
            'box_points': self.box_points,
            'time': self.time
        }
