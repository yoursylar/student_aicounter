from app import *
class Deterns(db.Model):
    
    id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    frame_id = db.Column(db.Integer)
    link = db.Column(db.String(1000), unique = True)
    x = db.Column(ARRAY(db.Integer))
    y = db.Column(ARRAY(db.Integer))

    def __init__(self, frame_id, link, x, y):
        self.frame_id = frame_id
        self.link = link
        self.x = x
        self.y = y
