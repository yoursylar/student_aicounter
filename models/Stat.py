from app import *
from datetime import datetime
class Stat(db.Model):
    __tablename__ = 'stats'
    
    #id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    zone_id = db.Column(db.Integer, db.ForeignKey('zones.id'))
    #date_column = db.Column(db.String(1000))
    #time_column = db.Column(db.String(1000))
    event_time = db.Column(db.DateTime, primary_key=True, nullable=False, default=datetime.now())
    people_count = db.Column(db.Integer)

    def __init__(self, zone_id, event_time, count):
        self.zone_id = zone_id
        self.event_time = event_time
        self.people_count = count

    def serialize(self):
        return{
            'zone_id': self.zone_id,
            'event_time': self.event_time,
            'people_count': self.people_count 
        }
