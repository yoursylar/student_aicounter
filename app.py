from flask import Flask, render_template, request, flash, jsonify
from sqlalchemy.dialects.postgresql import ARRAY
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

from sqlalchemy.sql import *
from models import *
import api

import os


app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/aicounter_last'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)

@app.route('/')
def hello_world():
    return render_template('new_index.html')
    #return render_template('unauthorised.html', found='yes')


@app.route('/auth')
def auth():
    return render_template('authorised.html', username='Pavel')
